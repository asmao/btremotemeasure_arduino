# Arduino Remote Measure (arduino scripts)
In this repository, Arduino script examples are provided to
illustrate how to interface the Arduino Remote Measure
android app with Arduinos.

## Materials and Methods
We focus on the following two Arduino models:
- Arduino Uno
- Arduino Nano

### Other Materials
- HC-05 Bluetooth Module
- Pushbutton switch
- 10 kΩ resistor

### Procedure
In this example, an Arduino sends times when
a pushbutton has been turned on (1) and when
this pushbutton has been turned off (0).

The resulting graph indicates the times 
where the button has been activated and 
where it has been deactivated.


#### Note for the Arduino Mega
The `Software Serial`
library is used for both the Uno and the Nano. In the case of the Arduino Mega, it is strongly 
recommended to make use of its multiple Serial interfaces.