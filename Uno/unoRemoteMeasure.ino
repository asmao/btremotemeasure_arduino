#include <SoftwareSerial.h>
SoftwareSerial Bluetooth(10, 9); //RX, TX
const int ledPin = 7;
const int buttonPin = 2;
int buttonState = 0;

void setup() {
  // put your setup code here, to run once:
  Bluetooth.begin(9600);
  Serial.begin(9600);
  // Lines that begin with # indicate comments
  // Data lines will follow a CSV format:
  // time, measurement [1], [2], [3], ...<\n>
  Bluetooth.println("#Time, New Button Status");
}

void loop() {
  // put your main code here, to run repeatedly:
  button();
}

void button(){
    if (buttonState ^ digitalRead(buttonPin)){
      Bluetooth.print(millis());
      Serial.print(millis());
      Serial.print(" ");
      if (buttonState == HIGH){
        Serial.println("Switch deactiviated");
        Bluetooth.print(", ");
        Bluetooth.println("0");
      }
      else {
        Serial.println("Switch activiated");
        Bluetooth.print(", ");
        Bluetooth.println("1");
      }
      buttonState = !buttonState;
  }
}

